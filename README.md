# Transmission Toolkit

[Documentation](https://treangenlab.gitlab.io/transmissiontoolkit/)

## Table Of Contents
* [Introduction](#introduction)
* [Features](#features)
  * [VCF Parsing](#vcf-parsing)
  * [Phylogenetic Trees](#phylogenetic-tree)
  * [Figure Creation](#figure-creation)
  * [Bottleneck Estimate](#bottleneck-estimate)
* [How to Use](#how-to-use)
* [Requirements](#requirements-for-github-download-&-use)
* [Contributors](#contributors)
* [References](#references)

## Introduction
Transmission Toolkit is a Python library coupled with a Jupyter Notebook environment demonstrating usage of the library. The library has many functionalities to help the user perform [phylogenetic](#phylogenetic-trees), [shared variant](#figure-creation), and [bottleneck](#bottleneck-estimate) analyses; all of which can be performed in conjunction with each other to provide insight into potential transmission events.

The Transmission Toolkit is flexible in how it parses VCFs and extracts variant information for subsequent analysis. Depending on the user's preferences, it can extract variants with a low or high frequency; with a read depth that exceeds a certain threshold; and/or that are not located at erroneous sites based on a mask file. 

The Transmission Toolkit can also produce 5 different visualizations to assist in shared variant analysis. These figures can provide insights into the frequency and location of shared variants, either between a pair of samples or among an entire data set. Some figures also allow users to easily compare analysis results before and after applying masking. 

The Transmission Toolkit also can assist in phylogenetic analysis. The toolkit uses RAxML to infer the phylogenetic tree from a data set, and Toytree/Toyplot to visualize the tree. Heat maps highlighting the locations of SNPs or iSNVs within sequences can also be produced alongside the trees.

Finally, the Transmission Toolkit offers use of the [BB bottleneck](https://github.com/weissmanlab/BB_bottleneck) software. The toolkit generates the input files necessary to run the software and generate a bottlneck size estimation of any pair of samples from your data.

All of these functionalities are packaged together in one place, including a Jupyter Notebook showcasing the functions of the toolkit on a sample set of data - all you have to do is click run!

To use your own data, follow the instructions in the [How to Use](#how-to-use) section.

## Features
### VCF Parsing
The Transmission Toolkit parses input VCF files as neccessary, to analyze data between pairs, to create phylogenetic trees, to create bottleneck estimation input, and more. This is all done automatically when you chose a function to perform. There are many parameters that users can alter to their liking to customize the type of variants that are extracted from the VCFs.
#### Parameters
- min_AF: minimum frequency to detect a variant, useful for filtering potential errors
- min_read_depth: minimum number of reads required to support a variant, useful for filtering potential errors
- max_AF: maximum frequency to include in parse, useful for extracting low frequency variants
- masks: a txt file with a list of erroneous positions you wish to mask
- parse_type: either 'biallelic' or 'multiallelic', handles the case of when there are multiple variants at the same position
- store_reference: a boolean that determines if the reference allele is treated as a variant in certain cases

### Phylogenetic Trees
The Transmission Toolkit uses RAxML to generate the most likely phylogenetic tree out of the inputted data, as well as Toyplot and Toytree to visualize the phylogenetic trees created by RAxML. Some parameters users can input to customize the tree visualization include focusing on small clusters, looking at low frequency variants to determine likely transmission pairs, and more.
#### Examples
##### Phylogenetic Tree
Calling `PhyloTree.visualize_tree()` results in the following figure:
![phylotree](images/phylotree.png)
Parameters:
- rtre: a Toytree instance or, ideally, the output of `PhyloTree.construct_tree()` which will add features (such as cluster color) to the tree nodes.
##### Phylogenetic Tree + SNP Heat Map 
Calling `PhyloTree.visualize_snp_heatmap()` results in the following figure: 
![phylotree_snp](images/phylotree_snp_heatmap.png)
Parameters: 
- rtre: a Toytree instance or, ideally, the output of `PhyloTree.construct_tree()` which will add features (such as cluster color) to the tree nodes.
##### Phylogenetic Tree + iSNV Heat Map 
Calling `PhyloTree.visualize_isnv_heatmap()` results in the following figure.
![phylotree_isnv](images/phylotree_isnv_heatmap.png)
For the full visualization, download the file [HERE](images/raxml_consensus_isnv.html).

Parameters: 
- rtre: a Toytree instance or, ideally, the output of `PhyloTree.construct_tree()` which will add features (such as cluster color) to the tree nodes.
- output: if set to "html", produces an HTML representative of the figure.
##### Individual iSNV Cluster Visualization
Calling `PhyloTree.visualize_isnv_clusters()` results in the following figures: 
![clusters1](images/phylotree_isnv_cluster_1.png)
![clusters2](images/phylotree_isnv_cluster_2.png)
![clusters3](images/phylotree_isnv_cluster_3.png)

### Plot Creation
The Transmission Toolkit uses matplotlib to create a multitude of figures analyzing the data, including analyzing low frequency shared variant between possible transmission pairs, showing the number of pairs that share a certain variant, illustrating bottleneck sizes of possible transmission pairs, and more. All figures have the parsing parameters available as well, to control how the data is formed before it is displayed in a figure.
#### Examples
##### Standard Allele Frequency Bar Plot
Calling `make_bar_plot(..., plot_type='standard', ...)` produces the following figure, which shows the frequency of variants shared between two samples.
![standardbarplot](images/standard_allele_freq.png)
Parameters:
- donor_path: relative/absolute path to the donor's vcf file
- recipient_path: relative/absolute path to the recipient's vcf file
- save: (optional) Boolean. Whether to save this visualization as a PNG file.
- output_folder: (optional) Relative/absolute path to the folder in which the allele frequency chart will be saved. 
- plot_type: (optional) 'standard' or 'weighted'. Determines if thickness of bars is weighted by number of reads or if it is left standard
##### Weighted Allele Frequency Bar Plot
Calling `make_bar_plot(..., plot_type='weighted', ...)` produces the following figure, which shows the frequency of variants shared between two samples, with weighted bars representing the amount of reads supporting each variant.
![weightedbarplot](images/weighted_allele_freq.png)

##### Number of Shared Variants Plot
Calling `masked_shared_variants()` produces the following figure, which shows the number of sample pairs sharing a certain number of variants when positions are both masked or unmasked.
![numsharedvariants](images/msv_chart.png)
Parameters:
- sv_count: One of the outputs of `sv_counter()`. A dictionary in the form `{number of shared variants: number of pairs with that number of shared variants}`.
- important_pairs: One of the outputs of `sv_counter()`. A list of pairs that have the threshold amount of shared variants or higher
- save: (optional) Boolean. Whether to save this visualization as a PNG file.
- output_folder: (optional) Relative/absolute path to the folder in which the 
  masked shared variants chart will be saved.

##### Top Shared Variant Positions Plot
Calling `shared_positions()` produces the following figure, which shows the positions that have the most pairs with a variant at that position
![positions](images/positions_chart.png)
Parameters:
- position_count: output from `position_counter()`; a dictionary in the form 
		`{position: number of pairs with a variant at position}`
- mask_file: relative/absolute path to a file listing positions to mask
- input_folder: relative/absolute path to a folder containing VCFs
- save: (optional) Boolean. Whether to save this visualization as a PNG file.
- output_folder: (optional) Relative/absolute path to the folder in which the 
  masked shared variants chart will be saved. 
- shown_variants: (optional) Number of most common variants to plot

### Bottleneck Estimate
The Transmission Toolkit uses the beta-binomial method (via BB bottleneck library) to calculate and visualize bottleneck size estimates of possible transmission pairs. 

#### Examples
##### Bottleneck Size Input File
Calling `bb_file_writer()` provides a `.txt` file to use as input to the bb_bottleneck software.
![bottleneckinput](images/bb_file_writer.png)
##### Bottleneck Size Estimation
This is the output from running BB Bottleneck's R script (provided in the toolkit) using the above text file (the screenshot) as input.
```
Rscript Bottleneck_size_estimation_approx.r --file new_ERR4145419_ERR4145461_thred2_complete_nofilter_bbn.txt --plot_bool TRUE --var_calling_threshold 0.03 --Nb_min 1 --Nb_max 200 --confidence_level 0.95
```
![bottleneckestimate](images/approx_plot.jpg)
##### Bottleneck Size Estimates of All Pairs
Calling `all_pairs()` produces the following figure. The error bars represent the 95% confidence interval of the estimates.
![bottleneckpairs](images/bottleneck_all_pairs.png)

##### Bottleneck Size Estimates in Clusters
If the metadata for a group of samples is available as a `TSV` file, `all_pairs_clusters()` can be called to plot the bottleneck sizes of sample pairs within clusters as shown. The error bars are colored according to the cluster the pair of samples belong to (as defined in the metadata)
![bottleneckclusters](images/bottleneck_clusters.png) 

## How to Use
There are 2 ways to access transmission toolkit on your device.

[//]: # (### mybinder.org Link)
[//]: # (To access TransmissionToolkit via mybinder.org, simply click this [link]\(https://mybinder.org/v2/gh/marybrady722/transmissiontoolkit/master\). Alternatively, you can copy and paste this url into the browsaer of your choice.)

[//]: # (https://mybinder.org/v2/gh/marybrady722/transmissiontoolkit/master)

[//]: # (Once you reach the link, you should see a loading page that looks like this:)

[//]: # (![image]\(https://github.com/marybrady722/transmissiontoolkit/blob/master/images/binder4.jpg\))

[//]: # (Now, just wait until the binder loads. That screen should change to one that looks like this after the binder has loaded:)

[//]: # (![image]\(https://github.com/marybrady722/transmissiontoolkit/blob/master/images/binder1.jpg\))

[//]: # (Click on the folder called sample_data. That should bring you to a page that looks like this:)

[//]: # (![image]\(https://github.com/marybrady722/transmissiontoolkit/blob/master/images/binder2.jpg\))

[//]: # (From there, click on Sample Data.ipynb. You should reach a page that looks like this:)

[//]: # (![image]\(https://github.com/marybrady722/transmissiontoolkit/blob/master/images/binder3.jpg\))

[//]: # (Now that you're in the notebook, click the run button for each code block to run through and generate tha sample data!)

### Docker
To access TransmissionToolkit via Docker, follow the instructions below.

First, [install Docker](https://docs.docker.com/get-docker/) on your computer. 

Once Docker is installed on your machine, run the following command in the terminal:

    docker run -p 8888:8888 registry.gitlab.com/treangenlab/transmissiontoolkit
     
If none of the outputted URLS work, navigate to `localhost:8888` and enter the token as outputted in the terminal. You should reach a screen like this:
![docker](images/docker_notebook.png)
Navigate to `trakit/` and click on `Trakit Demo.ipynb` to view code examples using sample data. To use your own data, upload a folder of vcfs, change the inputs in the notebook, and run the cells.

### GitLab Download
To use TransmissionToolkit by downloading the source code and dependencies yourself on your machine, follow the instructions below.

First, ensure you have downloaded all of the [required packages and libraries.](#requirements-for-github-download-&-use))

Once you have all of the requirements, download all of the code from this repo. 

To access the Jupyter Notebook, first ensure you have [Jupyter](https://jupyter.org/install) installed on your machine. Change directories to this project folder within the terminal, then run the following command.

    jupyter notebook

Navigate to `trakit/` and click on `Trakit Demo.ipynb`. Run the code cells to use our toolkit on the provided sample data.

To add your own data, change the input folder in `Trakit Demo.ipynb`, and rerun the cells.

## Requirements (for GitHub download & use)
In order to successfully run the Transmission Toolkit on your machine, you will need Python version 3 and the following Python libraries:
- [NumPy](https://numpy.org/install/)
- [PyVCF](https://pypi.org/project/PyVCF/)
- [matplotlib](https://matplotlib.org/users/installing.html)
- [Toytree](https://toytree.readthedocs.io/en/latest/3-installation.html)
- [Toyplot](https://toyplot.readthedocs.io/en/stable/installation.html)

To use the bottleneck calculation function, you will need to install the BB Bottleneck Estimator's dependencies as specified [here](https://github.com/weissmanlab/BB_bottleneck). `Bottleneck_size_estimation_approx.r` is already included in this repository.

To use the phylogenetic tree generation and visualization features, you will need [RAxML](https://cme.h-its.org/exelixis/web/software/raxml/hands_on.html), [Parsnp](https://harvest.readthedocs.io/en/latest/content/parsnp/quickstart.html#download-install-run), and [HarvestTools](https://harvest.readthedocs.io/en/latest/content/harvest/quickstart.html#download-install-run). You must add the paths to these binaries to your `PATH` environment variable. The easiest way to do this is to move the binaries to a folder already on the path (ex. `cp raxmlHPC* ~/bin/`; `cp Parsnp-Linux64-v1.2/parsnp ~/bin/`; `cp harvesttools-Linux64-v1.2/harvesttools ~/bin/`). 


## Contributers
RA Leo Elworth

Qi Wang

Nicolae Sapoval

Mary Brady

Kaiyu Hernandez

Angela Hwang

Todd J Treangen

## References

Leonard, Ashley Sobel, et al. "Transmission bottleneck size estimation from pathogen deep-sequencing data, with an application to human influenza A virus." Journal of virology 91.14 (2017).


bb_bottleneck software

matplotlib docs

toytree docs

raxml docs

