FROM ubuntu:latest
ENV DEBIAN_FRONTEND=noninteractive

# Install R, Python, and packages 
# https://stackoverflow.com/a/51500666
RUN apt-get update \
	&& apt-get install -y --no-install-recommends build-essential \
		r-base \
		python3.7 python3-pip python3-dev \
		libcurl4-openssl-dev libssl-dev libxml2-dev wget \
		r-cran-tidyverse

RUN mkdir trakit
WORKDIR trakit/
COPY . .

# https://stackoverflow.com/questions/58269375/how-to-install-packages-with-miniconda-in-dockerfile
# https://stackoverflow.com/questions/57292146/problems-running-conda-update-in-a-dockerfile
RUN wget \
    https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
    && bash Miniconda3-latest-Linux-x86_64.sh -b \
    && echo "export PATH="/root/miniconda3/bin:$PATH"" >> ~/.bashrc \
    && /bin/bash -c "source ~/.bashrc" \
    && rm -f Miniconda3-latest-Linux-x86_64.sh 
ENV PATH /root/miniconda3/bin:$PATH
RUN conda config --add channels defaults
RUN conda config --add channels bioconda
RUN conda config --add channels conda-forge
RUN conda install -c conda-forge toytree
RUN conda install -c bioconda parsnp
RUN conda install -c bioconda harvesttools

RUN pip3 install -r requirements.txt
RUN pip3 install jupyter
RUN R -e "install.packages('argparse',dependencies=TRUE, repos='https://cran.r-project.org/')"

# https://cme.h-its.org/exelixis/web/software/raxml/hands_on.html
RUN wget https://github.com/stamatak/standard-RAxML/archive/refs/heads/master.zip
RUN unzip master.zip
WORKDIR standard-RAxML-master/
RUN make --debug=v -f Makefile.gcc
RUN cp raxmlHPC* /bin/
WORKDIR ../
RUN rm -r standard-RAxML-master master.zip


CMD ["jupyter", "notebook", "--port=8888", "--no-browser", "--ip=0.0.0.0", "--allow-root"]